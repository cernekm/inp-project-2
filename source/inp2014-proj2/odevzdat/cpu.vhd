-- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2014 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): Martin Cernek <xcerne01 AT stud.fit.vutbr.cz>
--            Zdenek Vasicek <xvasic11 AT stud.fit.vutbr.cz>
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (0) / zapis (1)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is
   
   -- Instruction types
   type inst_type is (inc_ptr, dec_ptr, inc_content, dec_content, loop_start, loop_end, print_content, read_content, halt, nop);
   signal inst_dec : inst_type;

   -- FSM states
   type fsm_state is (s_idle, s_fetch0, s_fetch1, s_decode, s_inc_ptr, s_dec_ptr, s_inc_content0, s_inc_content1, s_dec_content0,
                      s_dec_content1, s_loop_start0, s_loop_start1, s_loop_start2, s_loop_start3, s_loop_end0, s_loop_end1,
                      s_loop_end2, s_loop_end3, s_loop_end4, s_print_content0, s_print_content1, s_read_content0, s_read_content1, s_nop,  s_halt);
   signal pstate  : fsm_state;
   signal nstate  : fsm_state;

   -- Program counter signals
   signal pc_reg  : std_logic_vector(12 downto 0);
   signal pc_inc  : std_logic;
   signal pc_dec  : std_logic;
   signal pc_en_add : std_logic;   -- Adresovanie programu 

   -- Pointer to data
   signal ptr_reg  : std_logic_vector(12 downto 0);
   signal ptr_inc  : std_logic;
   signal ptr_dec  : std_logic;
   signal ptr_en_add : std_logic;   -- Adresovanie dat

   -- Instruction register
   signal inst_reg    : std_logic_vector(7 downto 0);
   signal inst_ldata  : std_logic;

   -- Loop counter
   signal cnt_reg        : std_logic_vector(7 downto 0);
   signal cnt_inc        : std_logic;
   signal cnt_dec        : std_logic;
   signal cnt_assign_one : std_logic;     -- Nastavenie cnt regista na jednotku

begin

-- ----------------------------------------------------------------------------
--                      Program counter
-- ----------------------------------------------------------------------------
   program_counter: process (RESET, CLK)
   begin
      if (RESET = '1') then
         pc_reg <= (others => '0');
      elsif (CLK'event) and (CLK = '1') then
         if (pc_inc = '1') then
            pc_reg <= pc_reg + 1;
         elsif (pc_dec = '1') then
            pc_reg <= pc_reg - 1;
         end if;
      end if;
   end process;

-- ----------------------------------------------------------------------------
--                      Pointer to data
-- ----------------------------------------------------------------------------
   pointer_to_data: process (RESET, CLK)
   begin
      if (RESET = '1') then
         ptr_reg <= "1000000000000";    -- Data su ulozene od adresy 0x1000
      elsif (CLK'event) and (CLK = '1') then
         if (ptr_inc = '1') then
            ptr_reg(11 downto 0) <= ptr_reg(11 downto 0) + 1;
         elsif (ptr_dec = '1') then
            ptr_reg(11 downto 0) <= ptr_reg(11 downto 0) - 1;
         end if;
      end if;
   end process;

-- ----------------------------------------------------------------------------
--                      MX1
-- ----------------------------------------------------------------------------
   DATA_ADDR <= pc_reg  when (pc_en_add = '1') else   -- Prepinanie adresovania
                ptr_reg when (ptr_en_add = '1')
                        else (others => 'Z');

-- ----------------------------------------------------------------------------
--                      Loop counter
-- ----------------------------------------------------------------------------
   loop_counter: process (RESET, CLK)
   begin
      if (RESET = '1') then
         cnt_reg <= (others => '0');
      elsif (CLK'event) and (CLK = '1') then
         if (cnt_inc = '1') then
            cnt_reg <= cnt_reg + 1;
         elsif (cnt_dec = '1') then
            cnt_reg <= cnt_reg - 1;
         elsif (cnt_assign_one = '1') then
            cnt_reg <= "00000001";
         end if;
      end if;
   end process;

-- ----------------------------------------------------------------------------
--                      Instruction register
-- ----------------------------------------------------------------------------
   instruction_register: process (RESET, CLK)
   begin
      if (RESET = '1') then
         inst_reg <= (others => '0');
      elsif (CLK'event) and (CLK = '1') then
         if (inst_ldata = '1') then
            inst_reg <= DATA_RDATA;
         end if;
      end if;
   end process;

-- ----------------------------------------------------------------------------
--                      Instruction decoding
-- ----------------------------------------------------------------------------
   decode_inst: process (inst_reg)
   begin
      case (inst_reg) is
         when X"3E" => inst_dec <= inc_ptr;       -- >
         when X"3C" => inst_dec <= dec_ptr;       -- <
         when X"2B" => inst_dec <= inc_content;   -- +
         when X"2D" => inst_dec <= dec_content;   -- -
         when X"5B" => inst_dec <= loop_start;    -- [
         when X"5D" => inst_dec <= loop_end;      -- ]
         when X"2E" => inst_dec <= print_content; -- .
         when X"2C" => inst_dec <= read_content;  -- ,
         when X"00" => inst_dec <= halt;          -- null
         when others => inst_dec <= nop;          -- no operation
      end case;
   end process;

-- ----------------------------------------------------------------------------
--                      FSM
-- ----------------------------------------------------------------------------

   -- FSM present state
   fsm_pstate: process (RESET, CLK)
   begin
      if (RESET = '1') then
         pstate <= s_idle;
      elsif (CLK'event) and (CLK = '1') then
         if (EN = '1') then
            pstate <= nstate;
         end if;
      end if;
   end process;

   -- FSM next state
   fsm_nstate: process (pstate, inst_dec, DATA_RDATA, OUT_BUSY, IN_VLD, IN_DATA, cnt_reg)
   begin
      DATA_WDATA <= (others => '0');  -- Kvoli odstraneniu latchu
      DATA_RDWR <= '0';
      DATA_EN <= '0';
      IN_REQ <= '0';
      OUT_DATA <= (others => '0');    -- Kvoli odstraneniu latchu
      OUT_WE <= '0';
      pc_inc <= '0';
      pc_dec <= '0';
      pc_en_add <= '0';
      ptr_inc <= '0';
      ptr_dec <= '0';
      ptr_en_add <= '0';
      cnt_inc <= '0';
      cnt_dec <= '0';
      inst_ldata <= '0';
      cnt_assign_one <= '0';

      case (pstate) is

         -- Idle
         when s_idle =>
            nstate <= s_fetch0;

         -- Instruction fetch
         when s_fetch0 =>
            pc_en_add <= '1';
            DATA_EN <= '1';
            DATA_RDWR <= '0';
            nstate <= s_fetch1;

         when s_fetch1 =>
            inst_ldata <= '1';
            nstate <= s_decode;

         -- Instruction decode
         when s_decode =>
            case (inst_dec) is
               when inc_ptr =>
                  nstate <= s_inc_ptr;
               when dec_ptr =>
                  nstate <= s_dec_ptr;
               when inc_content =>
                  nstate <= s_inc_content0;
               when dec_content =>
                  nstate <= s_dec_content0;
               when loop_start =>
                  nstate <= s_loop_start0;
               when loop_end =>
                  nstate <= s_loop_end0;
               when print_content =>
                  nstate <= s_print_content0;
               when read_content =>
                  nstate <= s_read_content0;
               when halt =>
                  nstate <= s_halt;
               when nop =>
                  nstate <= s_nop;
            end case;

         -- Halt
         when s_halt =>
            nstate <= s_halt;

         -- NOP
         when s_nop =>
            pc_inc <= '1';
            nstate <= s_fetch0;

         -- Pointer increment
         when s_inc_ptr =>
            ptr_inc <= '1';
            pc_inc <= '1';
            nstate <= s_fetch0;

         -- Pointer decrement
         when s_dec_ptr =>
            ptr_dec <= '1';
            pc_inc <= '1';
            nstate <= s_fetch0;

         -- Content increment
         when s_inc_content0 =>    -- Nacitanie hodnoty do RDATA
            ptr_en_add <= '1';
            DATA_EN <= '1';
            nstate <= s_inc_content1;

         when s_inc_content1 =>    -- Zapisanie hodnoty RDATA+1
            ptr_en_add <= '1';
            DATA_EN <= '1';
            DATA_RDWR <= '1';
            DATA_WDATA <= DATA_RDATA + 1;
            pc_inc <= '1';
            nstate <= s_fetch0;

         -- Content decrement
         when s_dec_content0 =>    -- Nacitanie hodnoty do RDATA
            ptr_en_add <= '1';
            DATA_EN <= '1';
            nstate <= s_dec_content1;

         when s_dec_content1 =>    -- Zapisanie hodnoty RDATA-1
            ptr_en_add <= '1';
            DATA_EN <= '1';
            DATA_RDWR <= '1';
            DATA_WDATA <= DATA_RDATA - 1;
            pc_inc <= '1';
            nstate <= s_fetch0;

         -- Content printing
         when s_print_content0 =>  -- Nacitanie hodnoty do RDATA
            ptr_en_add <= '1';
            DATA_EN <= '1';
            nstate <= s_print_content1;

         when s_print_content1 =>  -- Vypis na display
            if (OUT_BUSY = '0') then
               OUT_WE <= '1';
               OUT_DATA <= DATA_RDATA;
               pc_inc <= '1';
               nstate <= s_fetch0;
            else
               nstate <= s_print_content1;
            end if;

         -- Content reading
         when s_read_content0 =>  -- Pripravenie na nacitanie
            IN_REQ <= '1';
            nstate <= s_read_content1;

         when s_read_content1 =>  -- Nacitanie hodnoty z klavesnice
            if (IN_VLD = '1') then
               ptr_en_add <= '1';
               DATA_EN <= '1';
               DATA_RDWR <= '1';
               DATA_WDATA <= IN_DATA;
               pc_inc <= '1';
               nstate <= s_fetch0;
            else
               IN_REQ <= '1';
               nstate <= s_read_content1;
            end if;

         -- Loop start
         when s_loop_start0 =>   -- Nacitanie hodnoty do RDATA
            ptr_en_add <= '1';
            DATA_EN <= '1';
            pc_inc <= '1';
            nstate <= s_loop_start1;

         when s_loop_start1 =>   -- Porovnanie s 0 a nastavenie CNT na 1
            if (DATA_RDATA = 0) then
               cnt_assign_one <= '1';
               nstate <= s_loop_start2;
            else
               nstate <= s_fetch0;
            end if;

         when s_loop_start2 =>   -- Nacitanie instrukcie
            pc_en_add <= '1';
            DATA_EN <= '1';
            nstate <= s_loop_start3;

         when s_loop_start3 =>  -- while(CNT != 0)
            if (cnt_reg = 0) then
               nstate <= s_fetch0;
            else 
               if (DATA_RDATA = X"5B") then
                  cnt_inc <= '1';
               elsif (DATA_RDATA = X"5D") then
                  cnt_dec <= '1';
               end if;
               pc_inc <= '1';
               nstate <= s_loop_start2;
            end if;

         -- Loop end
         when s_loop_end0 =>   -- Nacitanie hodnoty do RDATA
            ptr_en_add <= '1';
            DATA_EN <= '1';
            nstate <= s_loop_end1;

         when s_loop_end1 =>   -- Porovnanie s 0
           if (DATA_RDATA = 0) then
               pc_inc <= '1';
               nstate <= s_fetch0;
            else
               cnt_assign_one <= '1';
               pc_dec <= '1';
               nstate <= s_loop_end2;
            end if;

         when s_loop_end2 =>
            pc_en_add <= '1';
            DATA_EN <= '1';
            nstate <= s_loop_end3;

         when s_loop_end3 =>  -- while(CNT != 0)
            if (cnt_reg = 0) then
               nstate <= s_fetch0;
            else 
               if (DATA_RDATA = X"5B") then
                  cnt_dec <= '1';
               elsif (DATA_RDATA = X"5D") then
                  cnt_inc <= '1';
               end if;
               nstate <= s_loop_end4;
            end if;

         when s_loop_end4 =>  -- Porovnanie CNT s 0
            if (cnt_reg = 0) then
               pc_inc <= '1';
            else
               pc_dec <= '1';
            end if;
            nstate <= s_loop_end2;

         -- Else
         when others =>
            nstate <= s_fetch0;

      end case;
   end process;

end behavioral;
