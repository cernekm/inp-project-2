Návrh počítačových systémov INP - Projekt číslo 2.   
==================================================
## Procesor s jednoduchou inštrukčnou sadou  

Cieľomm tohoto projektu je implementovať pomocou VHDL procesor s Von Neumannovou architektúrou, ktorý
bude schopný vykonáavať program napísaný v jazyku [BrainF*ck](http://en.wikipedia.org/wiki/Brainfuck). I keď tento jazyk používa iba osem
jednoduchých príkazov (inštrukcí), jedná sa o výpočetne úplnú sadu, pomocou ktorej je možné realizovať
ľubovolný algoritmus.
  
Kompletné zadanie sa nachádza na tomto [odkaze](https://wis.fit.vutbr.cz/FIT/st/course-files-st.php/course/INP-IT/projects/INP-proj2.pdf).